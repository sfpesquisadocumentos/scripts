#!/bin/sh

INSTALL_DIR=~/Projetos
SYNTAXNET_HOME=${INSTALL_DIR}/models/syntaxnet/

die( )
{
    local code
    
    code=${1}
    shift
    echo $*
    exit ${code}
}

install( )
{
    while [ "${#}" -ge "1" ]
    do
        apt-get install "${1}" -y || die 4 "${1}"
        shift
    done
}

apt( )
{
	sudo apt-get "$1" -y || die "$2" "$1"
}

aptinstall( )
{
	sudo apt-get install "$1" -y || die "$2" "$1"
}

# [ "`id -u`" -eq "0" ] || die 1 "Execute como usuario root."

cd "${INSTALL_DIR}"

apt update 2
apt upgrade 3
apt update 2

aptinstall vim 4
aptinstall build-essential 5
aptinstall openjdk-8-jdk 6

aptinstall git 7
aptinstall zlib1g-dev 8
aptinstall file 9
aptinstall swig 10
aptinstall python2.7 11
aptinstall python-dev 12
aptinstall python-pip 13
aptinstall python-mock 14

pip install --upgrade pip
pip install -U protobuf==3.0.0
pip install asciitree
pip install numpy

if [ ! -f "bazel-0.4.3-installer-linux-x86_64.sh" ]; then
	wget https://github.com/bazelbuild/bazel/releases/download/0.4.3/bazel-0.4.3-installer-linux-x86_64.sh \
		-O bazel-0.4.3-installer-linux-x86_64.sh

	chmod +x bazel-0.4.3-installer-linux-x86_64.sh
fi

if [ ! -x ~/bin/bazel ]; then
	./bazel-0.4.3-installer-linux-x86_64.sh --user
fi

git clone --recursive https://github.com/tensorflow/models.git

cd ${SYNTAXNET_HOME}/tensorflow
echo "\n\n\n\n\n\n\n\n" | ./configure
apt autoremove -y
apt clean

cd ${SYNTAXNET_HOME}/

# para versão 1.0.0 do TensorFlow
bazel test syntaxnet/... util/utf8/...

# para a versão 0.12.0 do TensorFlow...
#bazel test -c opt --copt=-mavx --copt=-mavx2 --copt=-mfma --copt=-mfpmath=both \
#       --copt=-msse4.2 -k --genrule_strategy=standalone \
#       --linkopt=-lrt --ignore_unsupported_sandboxing \
#       --test_verbose_timeout_warnings syntaxnet/... util/utf8/...


