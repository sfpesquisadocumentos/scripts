#!/bin/bash

. ./common.bash

setv

checkhome
checkmodel

PARAMS=256-0.08-3600-0.9-0
for SET in training tuning; do
  bazel-bin/syntaxnet/parser_eval \
    --model_path=models/brain_tagger/greedy/$PARAMS/model \
    --task_context=models/brain_tagger/greedy/$PARAMS/context \
    --arg_prefix=brain_tagger \
    --hidden_layer_sizes=256 \
    --input=$SET-corpus \
    --output=tagged-$SET-corpus \
    --graph_builder=greedy \
    --batch_size=128
 
done

