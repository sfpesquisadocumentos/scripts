#!/bin/bash

. ./common.bash

setv

checkhome
checkmodel

PARAMS=512x512-0.08-4400-0.85-4
for SET in training tuning; do
  bazel-bin/syntaxnet/parser_eval \
    --task_context=models/brain_parser/greedy/$PARAMS/context \
    --batch_size=128 \
    --hidden_layer_sizes=512,512 \
    --input=tagged-$SET-corpus \
    --output=parsed-$SET-corpus \
    --arg_prefix=brain_parser \
    --graph_builder=greedy \
    --model_path=models/brain_parser/greedy/$PARAMS/model
done

