#!/bin/bash

. ./common.bash

setv

checkhome
checkmodel

TASK_CONTEXT=~/Projetos/scripts/syntaxnet-scripts/train/pt/context.pbtxt

bazel-bin/syntaxnet/parser_trainer \
  --output_path=models \
  --task_context=${TASK_CONTEXT} \
  --arg_prefix=brain_tagger \
  --params=256-0.08-3600-0.9-0 \
  --training_corpus=training-corpus \
  --tuning_corpus=tuning-corpus \
  --compute_lexicon \
  --hidden_layer_sizes=256 \
  --graph_builder=greedy \
  --batch_size=128 \
  --learning_rate=0.08 \
  --decay_steps=3600 \
  --momentum=0.9 \
  --seed=0

