#!/bin/bash

INSTALL_DIR=~
SYNTAXNET_HOME=${INSTALL_DIR}/models/syntaxnet/

die( )
{
    local code
    
    code=${1}
    shift
    echo $*
    exit ${code}
}

install( )
{
    while [ "${#}" -ge "1" ]
    do
        apt-get install "${1}" -y || die 4 "${1}"
        shift
    done
}

apt( )
{
    sudo apt-get "$1" -y || die "$2" "$1"
}

aptinstall( )
{
    sudo apt-get install "$1" -y || die "$2" "$1"
}

pipinstall( )
{
	local errno

	errno="${1}"
	shift
	# sudo pip install $@ || die "$errno" $@
	pip install $@ || die "$errno" $@
}

# [ "`id -u`" -eq "0" ] || die 1 "Execute como usuario root."

cd "${INSTALL_DIR}"

#apt update 2
#apt upgrade 3
#apt update 2

# if [ "${ETAPA}" -eq "1" ]; then
#	aptinstall vim 4
#	aptinstall build-essential 5
#	aptinstall openjdk-8-jdk 6
#
#	aptinstall git 7
#	aptinstall zlib1g-dev 8
#	aptinstall file 9
#	aptinstall swig 10
#	aptinstall python2.7 11
#	aptinstall python-dev 12
#	aptinstall python-pip 13
#	aptinstall python-mock 14
#
#	pipinstall 15 --upgrade pip
#	pipinstall 16 -U protobuf==3.0.0
#	pipinstall 17 asciitree
#	pipinstall 18 numpy
#fi

#if [ "${ETAPA}" -eq "2" ]; then
	if [ ! -f "bazel-0.4.3-installer-linux-x86_64.sh" ]; then
	    wget https://github.com/bazelbuild/bazel/releases/download/0.4.3/bazel-0.4.3-installer-linux-x86_64.sh \
		-O bazel-0.4.3-installer-linux-x86_64.sh

	    chmod +x bazel-0.4.3-installer-linux-x86_64.sh
	fi

	if [ ! -x ~/bin/bazel ]; then
	    ./bazel-0.4.3-installer-linux-x86_64.sh --user
	fi
#	if [ ! -x /usr/local/bin/bazel ]; then
#	    sudo ./bazel-0.4.3-installer-linux-x86_64.sh
#	fi
#fi

#if [ "${ETAPA}" -eq "3" ]; then
	git clone --recursive https://github.com/tensorflow/models.git

	cd ${SYNTAXNET_HOME}/tensorflow
#	echo "\n\n\n\n\n\n\n\n" | 
./configure
#	apt autoremove -y
#	apt clean

	cd ${SYNTAXNET_HOME}/

	# para vers�o 1.0.0 do TensorFlow
#	bazel test syntaxnet/... util/utf8/...

	# para a vers�o 0.12.0 do TensorFlow...
	bazel test -c opt --copt=-mfpmath=both \
	       --copt=-msse4.2 -k --genrule_strategy=standalone \
	       --linkopt=-lrt --ignore_unsupported_sandboxing \
	       --test_verbose_timeout_warnings syntaxnet/... util/utf8/...
#fi

