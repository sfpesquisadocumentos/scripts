#!/bin/bash

# versao modificada do script "demo.sh", com as seguintes opcoes:
# permite desabilitar LOG

. ./common.bash

setv

# formato padrao
INPUT_FORMAT=stdin
LOG=""
STEP=3

help( )
{
    echo "`basename ${0}`: executa o pipeline POS tagging, parsing e apresentacao. Parametros:"
	echo "--conll           a entrada deve estar no formato CoNLL"
	echo "--noconll         a entrada deve ser uma sentenca em texto (default)"
    echo "--help            mostra esta mensagem"
    echo "--home DIR        informa local da instalacao do SyntaxNet"
	echo "--log             produz saidas do TensorFlow e SyntaxNet na stderr tambem"
	echo "--nolog           nao produz saidas do TensorFlow e SyntaxNet na stderr tambem (default)"
    echo "--model {dir}     informa o diretorio raiz dos modelos (default: ${MODEL_DIR}"
    echo "--step N          determina até que etapa o pipeline deve executar: 1=POS, 2=Parse, 3=tree (default)"
    echo "--verbose         habilita mensagens neste script"
    echo "--noverbose       desabilita mensagens neste script (default)"
}

while [ "${#}" -gt "0" ]
do
    v "${1}"
	case "${1}" in
	"--conll")
		INPUT_FORMAT=stdin-conll
		;;
	"--noconll")
		INPUT_FORMAT=stdin
		;;
    "--help")
        help
        die
        ;;
    "--home")
        shift
        sethome "${1}"
        v "${SYNTAXNET_HOME}"
        ;;
	"--log")
		LOG="--alsologtostderr"
		;;
	"--nolog")
		LOG=""
		;;
    "--model")
        shift
#        [ -d "${1}" ] || die 4 "${1} nao e um caminho existente"
        MODEL_DIR="${1}"
        v "${MODEL_DIR}"
        ;;
    "--step")
        shift
        STEP=${1}
        v "${STEP}"
        ;;
    "--verbose")
        setv
        ;;
    "--noverbose")
        unsetv
        ;;
	*)
		die 1 "Parametro ${1} desconhecido"
		;;
	esac
    shift
done

checkhome
v "SYNTAXNET_HOME='${SYNTAXNET_HOME}'"
checkmodel
v "MODEL_DIR='${MODEL_DIR}'"

if [ -e "${MODEL_DIR}/context.pbtxt" ]; then
    TASK_CONTEXT="${MODEL_DIR}/context.pbtxt"
elif [ -e "${MODEL_DIR}/context" ]; then
    TASK_CONTEXT="${MODEL_DIR}/context"
else
    die 2 "Falha localizando arquivo de parametro 'task_context'"
fi

v "TASK_CONTEXT='${TASK_CONTEXT}'"

#CMD1="$PARSER_EVAL \
#  --input=$INPUT_FORMAT \
#  --output=stdout-conll \
#  --hidden_layer_sizes=64 \
#  --arg_prefix=brain_pos \
#  --graph_builder=structured \
#  --task_context=${TASK_CONTEXT} \
#  --model_path=$MODEL_DIR/tagger-params \
#  --slim_model \
#  --batch_size=1024 \
#  ${LOG}"

CMD1="$PARSER_EVAL \
  --input=$INPUT_FORMAT \
  --output=stdout-conll \
  --hidden_layer_sizes=64 \
  --arg_prefix=brain_tagger \
  --graph_builder=structured \
  --task_context=${TASK_CONTEXT} \
  --model_path=$MODEL_DIR/tagger-params \
  --slim_model \
  --batch_size=1024 \
  ${LOG}"

CMD2="$PARSER_EVAL \
  --input=stdin-conll \
  --output=stdout-conll \
  --hidden_layer_sizes=512,512 \
  --arg_prefix=brain_parser \
  --graph_builder=structured \
  --task_context=${TASK_CONTEXT} \
  --model_path=$MODEL_DIR/parser-params \
  --slim_model \
  --batch_size=1024 \
  ${LOG}"

CMD3="bazel-bin/syntaxnet/conll2tree \
  --task_context=${TASK_CONTEXT} \
  ${LOG}"

if [ "${STEP}" -eq 1 ]; then
    v "Comando:
---
${CMD1}"
    ${CMD1}
elif [ "${STEP}" -eq 2 ]; then
    v "Comando:
---
${CMD1} | ${CMD2}"
    ${CMD1} | ${CMD2}
elif [ "${STEP}" -eq 3 ]; then
    v "Comando:
---
${CMD1} | ${CMD2} | ${CMD3}"
    ${CMD1} | ${CMD2} | ${CMD3}
fi


