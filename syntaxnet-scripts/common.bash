#!/bin/bash

[ -z "${SYNTAXNET_HOME}" ] && SYNTAXNET_HOME=/opt/syntaxnet/syntaxnet
[ -z "${PARSER_EVAL}" ] && PARSER_EVAL=bazel-bin/syntaxnet/parser_eval
[ -z "${MODEL_DIR}" ] && MODEL_DIR=syntaxnet/models/parsey_mcparseface

VERBOSE=0

die( )
{
    local code

    if [ "${#}" -gt "0" ]; then
        code=${1}
        shift
    else
        code=0
    fi
    if [ "${#}" -gt "0" ]; then
        echo ${*}
    fi
    exit $code
}

setv( )
{
    VERBOSE=1
}

unsetv( )
{
    VERBOSE=0
}

v( )
{
    [ "${VERBOSE}" -gt "0" ] && echo $@
}

sethome( )
{
    [ ! -z "${1}" -a -d "${1}" ] || die 3 "'${1}' nao e um caminho existente"
    SYNTAXNET_HOME=${1}
}

checkhome( )
{
    [ -z "${SYNTAXNET_HOME}" ] && die 4 "Informe o local de instalacao do SyntaxNet"
    [ -d "${SYNTAXNET_HOME}" ] || die 5 "'${1}' nao e um caminho para o SyntaxNet"
    cd "${SYNTAXNET_HOME}"
}

checkmodel( )
{
    [ -z "${MODEL_DIR}" ] && die 6 "Informe o local do idioma treinado"
    [ -d "${MODEL_DIR}" ] || die 7 "Caminho invalido para o idioma treinado"
}

